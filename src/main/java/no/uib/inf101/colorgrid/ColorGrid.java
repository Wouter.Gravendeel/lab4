package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ColorGrid implements IColorGrid{

  int rowCount;
  int columnCount;
  List<List<Color>> colorGrid;

  public ColorGrid(int rows, int cols) {
    /* 
    this.rowCount = numberOfRows;
    this.columnCount = numberOfColumns;


    List<Color> row = new ArrayList<Color>(numberOfColumns);
    List<List<Color>> gridOfColor = new ArrayList<List<Color>>(numberOfRows);

    for (int j = 0; j < numberOfColumns; j++) {
      row.set(j,null);
    }

    for (int i = 0; i < numberOfRows; i++) {
      gridOfColor.set(i,row);
    }
    
    this.colorGrid = gridOfColor;
    */
    List<List<Color>> out = new ArrayList<>();
        for (int i = 0; i < rows; i++) {
            out.add(new ArrayList<>());
            for (int j = 0; j < cols; j++) {
                out.get(i).add(null);
            }
        }
        this.colorGrid = out;
        this.rowCount = rows;
        this.columnCount = cols;
  }

  @Override
  public int rows() {
    return rowCount;
  }

  @Override
  public int cols() {

    return columnCount;
  }

  @Override
  public List<CellColor> getCells() {
    List<CellColor> flatArray = new ArrayList<CellColor>(); //rowCount*columnCount
    
    
    for (int i = 0; i < rowCount; i++) {
      
      // Inner loop
      for (int j = 0; j < columnCount; j++) {
        
        flatArray.add(new CellColor (new CellPosition (i,j), this.colorGrid.get(i).get(j))); //i*columnCount+j,
      }
    } 
    
    return flatArray;
  }

  @Override
  public Color get(CellPosition pos) {

    return colorGrid.get(pos.row()).get(pos.col());

  }

  @Override
  public void set(CellPosition pos, Color color) {
    
    this.colorGrid.get(pos.row()).set(pos.col(), color);

  }
  
}
