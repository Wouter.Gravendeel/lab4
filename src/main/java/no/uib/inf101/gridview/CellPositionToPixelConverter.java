package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter {
  Rectangle2D box;
  GridDimension gd;
  double margin;

  public CellPositionToPixelConverter(Rectangle2D boxInput, GridDimension gdInput, double marginInput) {
    this.box = boxInput;
    this.gd = gdInput;
    this.margin = marginInput;
  }

  public Rectangle2D getBoundsForCell (CellPosition coord){
    
    double boxX = box.getX();
    double boxY = box.getY();
    double boxHeight = box.getHeight();
    double boxWidth = box.getWidth();
    double numberRows = gd.rows();
    double numberCols = gd.cols();


    double cellHeight = (boxHeight - (numberRows+1)*this.margin)/numberRows;
    double cellWidth = (boxWidth - (numberCols+1)*this.margin)/numberCols;

    double cellY = boxY + this.margin + coord.row() * (this.margin + cellHeight);
    double cellX = boxX + this.margin + coord.col() * (this.margin + cellWidth);
    
    Rectangle2D rect = new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
     
    return rect;

    //Rectangle2D rect = new Rectangle2D.Double(100, 100, 200, 100);

  }
  // TODO: Implement this class
}
