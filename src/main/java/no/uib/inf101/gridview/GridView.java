package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;

import javax.swing.*;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.RenderableImage;
import java.text.AttributedCharacterIterator;
import java.util.List;
import java.util.Map;

public class GridView extends JPanel {
    IColorGrid grid;
    private static final double OUTERMARGIN = 30;
    private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

    public GridView(IColorGrid grid) {
        this.grid = grid;
        this.setPreferredSize(new Dimension(400, 300));
    }

    public void paintComponent(Graphics g) {
        Graphics2D graphic = (Graphics2D) g;
        this.drawGrid(graphic);
    }

    public void drawGrid(Graphics2D g) {
        g.setColor(MARGINCOLOR);
        Rectangle2D.Double boundingbox = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, this.getWidth() - (2 * OUTERMARGIN), this.getHeight() - (2 * OUTERMARGIN));
        g.fill(boundingbox);
        CellPositionToPixelConverter converter = new CellPositionToPixelConverter(boundingbox, this.grid, OUTERMARGIN);
        GridView.drawCells(g, grid, converter);
    }

    private static void drawCells(Graphics2D g, CellColorCollection cells, CellPositionToPixelConverter converter) {
        for (CellColor cell : cells.getCells()) {
            if (cell.color() == null) {
                g.setColor(Color.DARK_GRAY);
            } else {
                g.setColor(cell.color());
            }
            g.fill(converter.getBoundsForCell(cell.cellPosition()));
        }
    }
}